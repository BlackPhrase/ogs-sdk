/*
 * This file is part of OGS Engine
 * Copyright (C) 1996-2001 Id Software, Inc.
 * Copyright (C) 2018-2021, 2024 BlackPhrase
 *
 * OGS Engine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OGS Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
*/

/// @file

#pragma once

#define	FCVAR_ARCHIVE (1 << 0)	///< Set to cause it to be saved to vars.rc
#define	FCVAR_USERINFO (1 << 1)	///< Changes the client's info string
#define	FCVAR_SERVER (1 << 2)	///< Notifies players when changed

#define FCVAR_EXTDLL (1 << 3) ///< Defined by external DLL
#define FCVAR_CLIENTDLL (1 << 4) ///< Defined by the client DLL

#define FCVAR_PROTECTED (1 << 5) ///< It's a server cvar, but we don't send the data since it's a password, etc. Sends 1 if it's not bland/zero, 0 otherwise as value
#define FCVAR_SPONLY (1 << 6) ///< This cvar cannot be changed by clients connected to a multiplayer server
#define FCVAR_PRINTABLEONLY (1 << 7) ///< This cvar's string cannot contain unprintable characters (e.g., used for player name etc)
#define FCVAR_UNLOGGED (1 << 8) ///< If this is a FCVAR_SERVER, don't log changes to the log file/console if we are creating a log
#define FCVAR_NOEXTRAWHITESPACE (1 << 9) ///< Strip trailing/leading white space from this cvar

//#define	FCVAR_SERVERINFO	4	///< added to serverinfo when changed
//#define	FCVAR_NOSET		8	///< don't allow change from console at all,
							/// but can be set from the command line
//#define	FCVAR_LATCH		16	///< save changes until server restart

// Nothing outside the Cvar_*() functions should modify these fields!
typedef struct cvar_s
{
	const char *name; // TODO: non-const?
	/*const*/ char *string;
	int flags;
	//qboolean archive;		// set to true to cause it to be saved to vars.rc
	//qboolean server;		// notifies players when changed // TODO: qboolean info in QW (either userinfo or serverinfo)
	float value;
	struct cvar_s *next;
} cvar_t;
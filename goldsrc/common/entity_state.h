/*
 * This file is part of OGS Engine
 * Copyright (C) 1996-1997 Id Software, Inc.
 * Copyright (C) 2018-2019, 2021, 2023 BlackPhrase
 *
 * OGS Engine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OGS Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
*/

/// @file

#pragma once

#include <pm_shared/pm_info.h>

#include "mathlib.h"
#include "weaponinfo.h"

#define MAX_WEAPONS 64

/// entity_state_t is the information conveyed from the server
/// in an update message
typedef struct entity_state_s
{
	int		number;			///< edict index
	
	float msg_time;
	
	int messagenum; // all player's won't be updated each frame
	
	vec3_t	origin;
	vec3_t	angles;
	
	int		modelindex;
	int		frame;
	int		colormap;
	int		skin;
	int		effects;
	
	byte	eflags;			///< nolerp, etc
	
	int movetype;
	vec3_t velocity;
	
	float friction;
	float gravity;
	
	// Player-specific
	int team; // TODO
	int playerclass; // TODO
	int health; // TODO
	qboolean spectator; // TODO
	int weaponmodel; // TODO
	int gaitsequence; // TODO
	vec3_t basevelocity;
	int usehull;
	int oldbuttons;
	int onground; // -1 = in air, else pmove entity number
	int iStepLeft;
	float flFallVelocity;
	
	float fov; // TODO
	int weaponanim; // TODO
	
	vec3_t startpos; // TODO
	vec3_t endpos; // TODO
	
	float impacttime; // TODO
	float starttime; // TODO
	
	// For mods
	int iuser1; // TODO
	int iuser2; // TODO
	int iuser3; // TODO
	int iuser4; // TODO
	
	float fuser1; // TODO
	float fuser2; // TODO
	float fuser3; // TODO
	float fuser4; // TODO
	
	vec3_t vuser1; // TODO
	vec3_t vuser2; // TODO
	vec3_t vuser3; // TODO
	vec3_t vuser4; // TODO
} entity_state_t;

typedef struct clientdata_s
{
	vec3_t origin;
	vec3_t velocity;
	
	int viewmodel; // TODO
	vec3_t punchangle;
	
	int flags; // dead, gib, etc
	int waterlevel;
	int watertype;
	vec3_t view_ofs;
	float health; // cl.stats[STAT_HEALTH]
	
	int bInDuck;
	
	int weapons; // TODO
	
	int flTimeStepSound;
	int flDuckTime;
	int flSwimTime;
	float waterjumptime;
	
	float maxspeed;
	
	float fov; // TODO
	int weaponanim; // TODO
	
	int m_iId; // TODO
	
	int ammo_shells; // TODO
	int ammo_nails; // TODO
	int ammo_cells; // TODO
	int ammo_rockets; // TODO
	
	float m_flNextAttack;
	
	int tfstate; // TODO
	
	int pushmsec; // TODO
	
	int deadflag;
	
	char physinfo[MAX_PHYSINFO_STRING];
	
	// For mods
	int iuser1; // TODO
	int iuser2; // TODO
	int iuser3; // TODO
	int iuser4; // TODO
	
	float fuser1; // TODO
	float fuser2; // TODO
	float fuser3; // TODO
	float fuser4; // TODO
	
	vec3_t vuser1; // TODO
	vec3_t vuser2; // TODO
	vec3_t vuser3; // TODO
	vec3_t vuser4; // TODO
} clientdata_t;

typedef struct local_state_s
{
	entity_state_t playerstate;
	clientdata_t client;
	weapon_data_t weapondata[MAX_WEAPONS];
} local_state_t;